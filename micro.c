#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

#define fatal_msg		"error: fatal error"
#define	exec_msg		"error: cannot execute "
#define cd_arg_msg		"error: cd: bad argument"
#define cd_dir_msg		"error: cd: wrong dir "

typedef struct s_command
{
	int			fd[2];
	int			nb_args;
	char		**args;
}				t_command;

int ft_strlen(const char *str)
{
	int i = 0;
	while (str[i])
		i++;
	return (i);
}

void	print_error(char *error_msg, char *path)
{
	write(STDERR_FILENO, error_msg, ft_strlen(error_msg));
	write(STDERR_FILENO, path, ft_strlen(path));
	write(STDERR_FILENO, "\n", 1);
}

int	get_nb_cmds(char **argv)
{
	int nb_cmds;

	nb_cmds = 1;
	while (*argv && strcmp(*argv, ";") != 0)
	{
		if (!strcmp(*argv, "|"))
			nb_cmds++;
		argv++;
	}
	return (nb_cmds);
}

int get_nb_args(char **argv)
{
	int nb_args = 0;

	while (*argv && strcmp(*argv, "|") != 0 && strcmp(*argv, ";") != 0)
	{
		nb_args++;
		argv++;
	}
	return (nb_args);
}

void	exec_cd(t_command *cmd, char **argv)
{
	if (cmd->nb_args != 2)
	{
		print_error(cd_arg_msg, "");
	}
	else if (chdir(argv[1]) == -1)
	{
		print_error(cd_dir_msg, argv[1]);
	}
}

void	exec_cmd(t_command *cmd, char **argv, char **env)
{
	int i;

	cmd->args = malloc(sizeof(char *) * (cmd->nb_args + 1));
	cmd->args[cmd->nb_args] = NULL;
	i = 0;
	while (i < cmd->nb_args)
	{
		cmd->args[i] = argv[i];
		i++;
	}
	if (execve(cmd->args[0], cmd->args, env) == -1)
	{
		print_error(exec_msg, cmd->args[0]);
		exit(EXIT_FAILURE);
	}
	free(cmd->args);
}

void	close_fds(t_command *cmd, int *sum_args, int tmp_fd_in, int i, int nb_cmds)
{
	if (i == nb_cmds - 1 && nb_cmds > 1)
	{
		close(tmp_fd_in);
	}
	if (i < nb_cmds - 1)
	{
		close(cmd->fd[1]);
		*sum_args += cmd->nb_args + 1;
	}
	else
		*sum_args += cmd->nb_args;
}

int	run_commands(char **argv, int nb_cmds, char **env)
{
	int sum_args;
	int	tmp_fd_in = -1;
	int	i;
	int status = 0;
	pid_t pid;
	t_command cmd;

	i = 0;
	sum_args = 0;
	while (i < nb_cmds)
	{
		if (i != 0)
		{
			if (tmp_fd_in != -1)
				close(tmp_fd_in);
			tmp_fd_in = cmd.fd[0];
		}
		if (i < nb_cmds - 1)
		{
			if (pipe(cmd.fd) == -1)
			{
				print_error(fatal_msg, "");
				exit(EXIT_FAILURE);
			}
		}
		cmd.nb_args = get_nb_args(argv + sum_args);
		if (!strcmp(argv[sum_args], "cd"))
		{
			exec_cd(&cmd, argv + sum_args);
			close_fds(&cmd, &sum_args, tmp_fd_in, i, nb_cmds);
			continue ;
		}
		pid = fork();
		if (pid == -1)
		{
			print_error(fatal_msg, "");
			exit(EXIT_FAILURE);
		}
		else if (pid == 0)
		{
			if (i != 0 && dup2(tmp_fd_in, STDIN_FILENO) == -1)
			{
				print_error(fatal_msg, "");
				exit(EXIT_FAILURE);
			}
			if (i < nb_cmds - 1 && dup2(cmd.fd[1], STDOUT_FILENO) == -1)
			{
				print_error(fatal_msg, "");
				exit(EXIT_FAILURE);
			}
			exec_cmd(&cmd, argv + sum_args, env);
		}
		waitpid(pid, &status, 0);
		close_fds(&cmd, &sum_args, tmp_fd_in, i, nb_cmds);
		i++;
	}
	return (sum_args);
}

int main(int argc, char **argv, char **env)
{
	int i;
	int nb_cmds;

	(void)argc;
	i = 1;
	while (argv[i])
	{
		if (strcmp(argv[i], ";") != 0)
		{
			nb_cmds = get_nb_cmds(argv + i);
			i += run_commands(argv + i, nb_cmds, env) - 1;
		}
		i++;
	}
	return (EXIT_SUCCESS);
}