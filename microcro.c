#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <stdio.h>

#define fatal_msg	"error: fatal"
#define exec_msg	"error: cannot execute "
#define bad_arg_msg	"error: cd: bad arguments"
#define bad_dir_msg	"error: cd: cannot change directory to "

typedef struct	s_command
{
	char 				**args;
	int 				fd[2];
	int 				nb_args;
}				t_command;

int	ft_strlen(const char *str)
{
	int i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

void	print_error(char *error_msg, char *path)
{
	write(STDERR_FILENO, error_msg, ft_strlen(error_msg));
	write(STDERR_FILENO, path, ft_strlen(path));
	write(STDERR_FILENO, "\n", 1);
}

int get_nb_cmd(char **argv)
{
	int nb_cmd;

	nb_cmd = 1;
	while (*argv && strcmp(*argv, ";") != 0)
	{
		if (!strcmp(*argv, "|"))
		{
			nb_cmd++;
		}
		argv++;
	}
	return (nb_cmd);
}

int get_nb_args(char **argv)
{
	int nb_args;

	nb_args = 0;
	while (*argv && strcmp(*argv, ";") != 0 && strcmp(*argv, "|") != 0)
	{
		nb_args++;
		argv++;
	}
	return (nb_args);
}

void exec_cd(char **argv, int nb_args)
{
	if (nb_args != 2)
	{
		print_error(bad_arg_msg, "");
	}
	else if (chdir(argv[1]) == -1)
	{
		print_error(bad_dir_msg, argv[1]);
	}
}

void close_fds(t_command *cmd, int i, int *nb_args_sum, int nb_cmd, int fd_in_cmd)
{
	if (i == nb_cmd - 1 && nb_cmd > 1)
		close(fd_in_cmd);
	if (i < nb_cmd - 1)
	{
		close(cmd->fd[1]);
		*nb_args_sum += cmd->nb_args + 1;
	}
	else
		*nb_args_sum += cmd->nb_args;
}

void exec_cmd(char **argv, char **env, t_command *cmd)
{
	int i;

	cmd->args = malloc(sizeof(char *) * (cmd->nb_args + 1));
	//le dernier element du tableau est egal a NULL
	cmd->args[cmd->nb_args] = NULL;
	i = 0;
	//stocker les arguments de argv dans args
	while (i < cmd->nb_args)
	{
		cmd->args[i] = argv[i];
		i++;
	}
	//execution de args
	if (execve(cmd->args[0], cmd->args, env) == -1)
	{
		print_error(exec_msg, cmd->args[0]);
		exit(EXIT_FAILURE);
	}
	free(cmd->args);
}

int run_commands(char **argv, int nb_cmd, char **env)
{
	int i;
	int nb_args_sum;
	int fd_in_cmd = -1;
	int status = 0;
	t_command cmd;
	pid_t pid;

	i = 0;
	nb_args_sum = 0;
	//parcourir les differentes commandes separees par des pipes
	while (i < nb_cmd)
	{
		//verifier si un pipe a deja ete ouvert, si c'est la premiere execution d'une commande, on ne va pas
		//enter dans cette condition
		if (i != 0)
		{
			//si le pipe de lecture a deja ete ouvert, on va le fermer
			if (fd_in_cmd != -1)
				close(fd_in_cmd);
			//si on est deja dans un pipe, on va set le pipe de lecture a une copie du fd precedent
			fd_in_cmd = cmd.fd[0];
		}
		//ici on va verifier que la commande est bien suivie de pipes, on va ouvrir un nouveau pipe si c'est le cas
		if (i < nb_cmd - 1)
		{
			if (pipe(cmd.fd) == -1)
			{
				print_error(fatal_msg, "");
				exit(EXIT_FAILURE);
			}
		}
		//une fois dans la commande, on va verifier le nombre d'arguments qu'elle contient.
		cmd.nb_args = get_nb_args(argv + nb_args_sum);
		// nous allons verifier si un des premiers arguments de la commande est cd, si oui nous allons l'executer
		if (!strcmp(argv[nb_args_sum], "cd"))
		{
			exec_cd(argv + nb_args_sum, cmd.nb_args);
			// apres l'execution de cd, nous allons verifier si nous sommes a la fin d'une succession de pipes, si ou
			// nous allons fermer le dernier fd en lecture

			close_fds(&cmd, i, &nb_args_sum, nb_cmd, fd_in_cmd);
			//i++;
			continue ;

			//goto Jump;
		}
		//ici nous allons fork le processus
		pid = fork();
		//si le fork s'execute mal, retourner une erreur
		if (pid == -1)
		{
			print_error(fatal_msg, "");
			exit(EXIT_FAILURE);
		}
		//si le fork s'execute correctement executer le child process suivant
		else if (pid == 0)
		{
			//si on est pas au tout debut de la suite de pipes (donc d'autres pipes ont été ouverts), on dup2 le pipe in
			if (i != 0 && dup2(fd_in_cmd, STDIN_FILENO) == -1)
			{
				print_error(fatal_msg, "");
				exit(EXIT_FAILURE);
			}
			//si on est pas a la fin de la suite de pipes (donc d'autres pipes sont a ouvrir), on dup2 le pipe out
			if (i < nb_cmd - 1 && dup2(cmd.fd[1], STDOUT_FILENO) == -1)
			{
				print_error(fatal_msg, "");
				exit(EXIT_FAILURE);
			}
			//ici on va executer la commande qu'il reste
			exec_cmd(argv + nb_args_sum, env, &cmd);
		}
		//parent process : on va attendre que le child termine pour finir d'executer le parent
		waitpid(pid, &status, 0);
		close_fds(&cmd, i, &nb_args_sum, nb_cmd, fd_in_cmd);
		i++;
	}
	return (nb_args_sum);
}

int main(int argc, char **argv, char **env)
{
	int i;
	int nb_cmd;

	(void)argc;
	i = 1;
	while (argv[i])
	{
		if (strcmp(argv[i], ";") != 0)
		{
			nb_cmd = get_nb_cmd(argv + i);
			i += run_commands(argv + i, nb_cmd, env) - 1;
		}
		i++;
	}
	return (EXIT_SUCCESS);
}